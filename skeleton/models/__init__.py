from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy.types import String, Integer
from skeleton.app import app

from skeleton.utils import generate_uuid


db = SQLAlchemy(app)


class Users(db.Model):
    __tablename__ = 'users'

    id = db.Column(String, primary_key=True, default=generate_uuid)
    first_name = db.Column(String(length=256))
    last_name = db.Column(String(length=256))
    city = db.Column(String(length=256))
    karma = db.Column(Integer)

    def __repr__(self):
        return '<User {} {} from {}>'.format(
            self.first_name,
            self.last_name,
            self.city
        )


class Books(db.Model):
    __tablename__ = 'books'

    id = db.Column(String, primary_key=True, default=generate_uuid)
    owner_id = db.Column(String, db.ForeignKey('users.id'))
    subject = db.Column(String(length=256))
    author = db.Column(String, db.ForeignKey('author.id'))
    state = db.Column(String(length=256))

    def __repr__(self):
        return '<Book subject {}, author {}'.format(
            self.subject,
            self.author
        )

class Author(db.Model):
    __tablename__ = 'author'

    id = db.Column(String, primary_key=True, default=generate_uuid)
    first_name = db.Column(String(length=256))
    last_name = db.Column(String(length=256))

    def __repr__(self):
        return '<Author {} {}'.format(
            self.first_name,
            self.last_name
        )