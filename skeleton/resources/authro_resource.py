from skeleton.resources.base_resource import AuthorBaseResource
from skeleton.utils.validators import AuthorForm


class AuthorResourceList(AuthorBaseResource):
    pass

class AuthorResource(AuthorBaseResource):

    def post(self):
        form = AuthorForm(csrf_enabled=False)
        if not form.validate_on_submit():
            return 'Data validation error.', 422

        author_id = self.create_author(form.data)

        if not author_id:
            return 'Cannot create record because customer name already exists', 409

        return {'author_id': author_id}
