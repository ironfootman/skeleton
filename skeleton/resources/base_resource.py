from flask.ext.sqlalchemy import Model
from sqlalchemy.exc import ProgrammingError, IntegrityError

from flask_restful import Resource
from skeleton.models import db, Author


class BaseResource(Resource):
    def save_dict_as_model(self, data: dict, model: Model):
        m = model(**data)
        db.session.add(m)

        try:
            db.session.commit()
        except (ProgrammingError, IntegrityError):
            return None
        else:
            return m.id


class AuthorBaseResource(BaseResource):
    def create_author(self, author_data: dict):
        return self.save_dict_as_model(author_data, Author)