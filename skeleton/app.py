from flask import Flask
from flask.ext.restful import Api
from flask_cors import CORS

from skeleton.config import runtime_config


app = Flask(__name__)
app.config.from_object(runtime_config())

cors = CORS(app, resources={r"*": {"origin": "*"}})

api = Api(app, prefix="/bookshelf/v1")

from skeleton.resources.authro_resource import (
    AuthorResource,
    AuthorResourceList
)

api.add_resource(AuthorResource, '/author')


