import os

base_dir = os.path.abspath(os.path.dirname(__file__))

runtime_bk_env = os.environ.get("BK_ENV", "dev")
runtime_db_uri = os.environ.get("BK_DB_URI", "")

class Config:
    DEBUG = False
    TESTING = False
    HOST = '127.0.0.1'
    # this is dynamically overriden in `runtime_config` for Prod and QC
    SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/bookshelf.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = True



class DevConfig(Config):
    DEBUG = True
    TESTING = True

class ProdConfig(Config):
    pass

# this is not meant to be used with runtime_config, just straight import to tests
class TestConfig(DevConfig):
    SQLALCHEMY_DATABASE_URI = runtime_db_uri or "sqlite:///:memory:"


ENV_CONFIG = {
    'dev': DevConfig,
    'prod': ProdConfig,
}

def runtime_config():
    config = DevConfig

    # we assume static local dev config as well
    if runtime_bk_env == 'dev':
        return DevConfig

    if runtime_bk_env == 'prod':
        config = ProdConfig

    return config