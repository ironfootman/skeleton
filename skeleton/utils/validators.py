from flask_wtf import Form
from wtforms import StringField
from wtforms.validators import Length, AnyOf, InputRequired, Optional, Regexp


# class ValidateIf:
#     """
#     Validators proxy, that allows apply validators (generally, InputRequired or Optional) on given field by some
#     condition. Guarantee works with StringField and BooleanField.
#     """
#     def __init__(self, fieldname, fielddata, positive=InputRequired, negative=Optional):
#         self.fieldname = fieldname
#         self.fielddata = fielddata
#         self.positive = positive
#         self.negative = negative
#
#     def __call__(self, form, field):
#         field_for_process = getattr(form, self.fieldname, None)
#         field_for_process_data = field_for_process.data
#
#         if field_for_process and field_for_process_data == self.fielddata:
#             self.positive(form, field)
#         else:
#             self.negative(form, field)


class AuthorForm(Form):
    first_name = StringField('first_name', validators=[InputRequired(),
                                                       Length(min=3, max=256),
                                                       Regexp('^[A-z0-9].*$')])
    last_name = StringField('last_name', validators=[InputRequired(),
                                                       Length(min=3, max=256),
                                                       Regexp('^[A-z0-9].*$')])